import React from "react";
import {
    BrowserRouter as Router,
    Link,
    Route
} from "react-router-dom";

import AddNews from './AddNews';
import AboutUs from './AboutUs';
import News from './News';
import Home from './Home'


const Header = () => {
    return (
        <Router>
            <header id="header" className="fixed-top">

                <div className="container d-flex align-items-center">

                    <h1 className="logo mr-auto"><a href="#home">روزنامه</a></h1>

                    <nav className="nav-menu d-none d-lg-block">

                        <ul>
                            <li><Link to="/addnews"><p>افزودن</p></Link></li>
                            <li><Link to="/news"><p>اخبار</p></Link></li>
                            <li><Link to="/aboutus"><p>درباره ما</p></Link></li>
                            <li><Link to="/"><p>خانه</p></Link></li>
                        </ul>

                    </nav>
                </div>
            </header>
            <div className="mt-5 pt-5">
                <Route exact path="/" component={Home}></Route>
                <Route exact path="/aboutus" component={AboutUs}></Route>
                <Route exact path="/news" component={News}></Route>
                <Route exact path="/addnews" component={AddNews}></Route>
            </div>
        </Router>
    )
}

export default Header