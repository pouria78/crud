import axios from "axios";
import React, { useEffect, useState } from "react";

import { FiEdit } from "react-icons/fi";
import { TiDeleteOutline } from "react-icons/ti";

import { Modal, Button } from 'react-bootstrap';

const News = () => {
    const [news, setNews] = useState({ posts: [] })
    const getData = () => {
        axios
            .get("http://localhost:8000/posts")
            .then(getposts => {
                setNews({ posts: getposts.data })
            })
            .catch(err => console.log(err))
    }

    useEffect(() => {
        getData()
    }, [setNews])

    const deletepost = (e, post) => {
        console.log(news)
        const newPosts = news.posts.filter(item => item.id !== post.id)
        setNews({ posts: newPosts })
        axios
            .delete(`http://localhost:8000/posts/${post.id}`)
            .catch(err => console.log(err))
    }

    const makeModal = (e, post) => {
        const handleSaveChanges = (post) => {
            const editedBodyModal = document.getElementById("bodyModal").innerHTML
            const editedTitleModal = document.getElementById("titleModal").innerHTML
            post.message = editedBodyModal
            post.subject = editedTitleModal
            // setNews({posts : news})
            const newsToEdit = (news.posts || []).find((item) => item.id === post.id)
            console.log(newsToEdit);
            handleClose()
        }
        return (
            <Modal show={show} onHide={handleClose}>
                <Modal.Header className="HeaderModal" closeButton>
                    <Modal.Title contenteditable="true">
                        <span id="titleModal" >
                            {post.subject}
                        </span>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body contenteditable="true" >
                    <span
                        id="bodyModal">
                        {post.message}
                    </span>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        بستن
                </Button>
                    <Button variant="primary" onClick={handleSaveChanges}>
                        ذخیره تغییرات
                </Button>
                </Modal.Footer>
            </Modal>
        )
    }

    const [show, setShow] = useState(false);
    const [elemet, setElement] = useState()
    const [poststate, setPostState] = useState()
    const handleClose = () => setShow(false);
    const handleShow = (e, post) => {
        setShow(true)
        setElement(e)
        setPostState(post)
    }
    return (
        <>
            {show ? makeModal(elemet, poststate) : ''}
            <section dir="rtl" id="why-us" className="why-us">
                <div className="container">
                    <div className="row">
                        <div className="d-flex align-items-stretch">
                            <div className="icon-boxes d-flex flex-column justify-content-center">
                                <div className="row d-flex justify-content-start">
                                    {
                                        news.posts &&
                                        (news.posts || []).map(post =>
                                        (
                                            <div className="icon-box-upper 
                                                col-xl-3 d-flex align-items-stretch"
                                                key={post.id}>
                                                <div className="icon-box mt-4 mt-xl-0">
                                                    <FiEdit
                                                        onClick={e => handleShow(e, post)}
                                                        className="editIcon"
                                                    />
                                                    <TiDeleteOutline
                                                        onClick={e => deletepost(e, post)}
                                                        className="deleteIcon"
                                                    />
                                                    <i className="bx bx-cube-alt"></i>
                                                    <h5>{post.subject}</h5>
                                                    <p>{post.message}</p>
                                                </div>
                                            </div>)
                                        )
                                    }
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
        </>
    )
}

export default News