import React from 'react';
import { AiFillSetting , AiFillSound,AiFillBulb} from "react-icons/ai";

const AboutUs = () => {
    return (
    <section id="about" className="about text-right">
      <div className="container float-right">

          <div className="col-xl-10 col-lg-6 icon-boxes d-flex flex-column align-items-stretch 
          justify-content-center  px-lg-5">
            <h3>پایگاه خبری-تحلیلی</h3>
            <p className="text-justify-center">صاحب امتیاز و مدیر مسئول: حمیدرضا بهرامی
دارای مجوز شماره 75886 از وزارت فرهنگ و ارشاد اسلامی
تجارت‌نیوز متعلق به بخش خصوصی است و وابسته به هیچ ارگان یا سازمان دولتی و حاکمیتی نیست.</p>

            <div className="icon-box">
                <div className="icon float-right ml-3"><i className="bx bx-fingerprint"><AiFillSetting /></i></div>
              <h4 className="title"><a href="#ss">قانون‌مندی و تعادل</a></h4>
              <p className="description">محتوا و اخبار این رسانه، تحت نظارت تیمی کارشناس و حرفه‌ای تولید می‌شود</p>
              
            </div>

            <div className="icon-box">
              <div className="icon float-right ml-3"><i className="bx bx-gift"><AiFillBulb /></i></div>
              <h4 className="title"><a href="#ss">شفافیت و استقلال رای</a></h4>
              <p className="description">به قوانین جاری کشور پایبندیم و جانب تعادل را نگاه می‌داریم</p>
            </div>

            <div className="icon-box">
              <div className="icon float-right ml-3"><i className="bx bx-atom"><AiFillSound /></i></div>
              <h4 className="title"><a href="#ss">تخصص</a></h4>
              <p className="description">از گردش آزاد اطلاعات حمایت می‌کنیم و حق استقلال رای را برای خود قائل هستیم</p>
            </div>

          </div>
      </div>
    </section>
    )
}

export default AboutUs