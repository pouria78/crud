import React, { useRef, useEffect } from 'react';
import { useHistory } from "react-router-dom";
import Axios from "axios"

const AddNews = () => {
    const History = useHistory()
    const userName = useRef()
    const userEmail = useRef()
    const userSubject = useRef()
    const userMessage = useRef()

    const handleAddtoNews = (event) => {
        event.preventDefault()
        const addednews = {
            userName: userName.current.value,
            userEmail: userEmail.current.value,
            userSubject: userSubject.current.value,
            userMessage: userMessage.current.value
        }
        Axios.post("http://localhost:8000/posts",{
            "username": addednews.userName,
            "useremail": addednews.userEmail,
            "subject": addednews.userSubject,
            "message" : addednews.userMessage
        })
        .then(()=>{
            History.push("/news")
        })
        .catch(err => console.log(err))

        
    }

    useEffect(() => {
        console.log("component did mounted!!!");
    }, [])



    return (
        <section id="contact" className="contact">
            <div className="container">

                <div className="section-title">
                    <h2>افزودن خبر جدید</h2>
                </div>

            </div>

            <div className="container ">
                <div className="row mt-5 d-flex justify-content-center ">

                    <div className="col-lg-8 mt-5 mt-lg-0">

                        <form onSubmit={handleAddtoNews} className="php-email-form">
                            <div className="form-row">
                                <div className="col-md-6 form-group">
                                    <input type="email" className="form-control" name="email" id="email" ref={userEmail} placeholder="ایمیل شما" data-rule="email" data-msg="Please enter a valid email" />
                                    <div className="validate"></div>
                                </div>
                                <div className="col-md-6 form-group">
                                    <input type="text" name="name" className="form-control" id="name"
                                        ref={userName} placeholder="اسم شما" data-rule="minlen:4"
                                        data-msg="Please enter at least 4 chars"
                                    />
                                    <div className="validate"></div>
                                </div>
                            </div>
                            <div className="form-group">
                                <input type="text" className="form-control" name="subject" id="subject" ref={userSubject} placeholder="موضوع" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                                <div className="validate"></div>
                            </div>
                            <div className="form-group">
                                <textarea className="form-control" name="message" ref={userMessage} rows="5" data-rule="required" data-msg="Please write something for us" placeholder="لطفا متن خبر را بنویسید"></textarea>
                                <div className="validate"></div>
                            </div>
                            <div className="mb-3">
                                <div className="loading">Loading</div>
                                <div className="error-message"></div>
                                <div className="sent-message">Your message has been sent. Thank you!</div>
                            </div>
                            <div className="text-center">
                                <button type="submit">ارسال خبر</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default AddNews