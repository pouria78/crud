import Header from './components/Header';

import 'bootstrap/dist/css/bootstrap.min.css';
import "./assets/styles/style.css";

function App() {
  return (
    <div>
      <Header />
    </div>
  );
}

export default App;